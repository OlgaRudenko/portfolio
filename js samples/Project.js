'use strict';
import fs from 'fs';
import archiver from 'archiver';
import Router  from 'router';
import BaseCtrl from './index';
import AccessMiddleware    from 'lib/access';
import helpers            from 'lib/helpers';

import Project           from 'model/Project';
import User              from 'model/User';
import Attachment        from 'model/Attachment';
import ProjectStage      from 'model/ProjectStage';
import ProjectDocument   from 'model/ProjectDocument';
import Contract          from 'model/Crowdsale/Contract';

import config from '../config';

const router = new Router();

const CtrlName = 'Project';

const DOCUMENTS_BASE_PATH = process.env.DOCUMENTS_BASE_PATH || '/usr/src';

const permissions = {
  group: CtrlName,
  Create: {key: `${CtrlName}Create`, role: ['admin', 'developer']},
  Update: {key: `${CtrlName}Update`, role: ['admin', 'developer']},
  Delete: {key: `${CtrlName}Delete`, role: ['admin',]},
  GetDocs: {key: `${CtrlName}GetDocs`, role: 'admin'},
};

const POPULATES_LIST = {
  'user': {
    model: User.model,
    as: 'userData',
    attributes: ['id', 'firstname', 'lastname', 'username', 'email', 'company', 'link'],
    include: [{
      model: Attachment.model,
      as: 'thumbData',
      attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']}
    }]

  },
  'images': {
    model: Attachment.model,
    as: 'images',
    through: {attributes: []},
    attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']}
  },
  'documents': {
    model: ProjectDocument.model,
    as: 'documents',
    attributes: ['id', 'name', 'language', 'type'],
    include: [{
      model: Attachment.model,
      as: 'attachmentData',
      attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']}
    }]
  },
  'stages': {
    model: ProjectStage.model,
    as: 'stages',
    attributes: ['id', 'name', 'description', 'status']
  },
  'contract': {
    model: Contract.model,
    as: 'contractData'
  }
};

function beforeRouteGetAll(req, res, next, formData) {
  let user = req.user;
  let isAdmin = user && user.role == 'admin';
  if (!isAdmin) {
    formData.where = formData.where || {};
    formData.where.approved = true;
  }
}

function wrapHandlerCreateUpdate(req, res, next, Model, type = 'create') {
  const formData = Object.assign(req.formData, {approved: undefined});
  let user = req.user;
  let isAdmin = user && user.role == 'admin';

  const populate_list = [];
  let include = helpers.addPopulatesToFormData(POPULATES_LIST, populate_list);
  return User.get(user.id, {include})
    .then(user => {
      if (!isAdmin) {
        // if(!(user.id||{}).approved){
        //   return Promise.reject({ status: 403, message: "Current user wasn't approved to create projects"});
        // }
        formData.userId = user.id;
      }

      let hardcap = formData.hardcap;
      let totalSupply = formData.totalSupply;
      formData.tokenPriceUsd = hardcap/totalSupply;

      let options = [formData];

      if (type === 'update') {
        options.unshift(req.item.id);
      }
      return Model[type](...options)
    })
    .then(project => {
      if (formData.imageIds) {
        return project.setImages(formData.imageIds).then(data => project);
      }
      return project;
    })
    .then(project => {
      let docs = req.formData.documents;
      if (docs) {
        // save docs for project
        return Promise.all(docs.filter(doc=>!doc.projectId).map(doc => ProjectDocument.create({...doc, projectId: project.id})))
          .then(() => {
            return project;
          });
      }
      // no docs, next step
      return project;
    })
    .then(project => {
      res.json(project);
    })
    .catch(err => next(err));
}

function handlerCreate(...options) {
  return wrapHandlerCreateUpdate.apply(null, options);
}

function handlerUpdate(...options) {
  options.push('update');
  return wrapHandlerCreateUpdate.apply(null, options);
}

function handlerGetAll(req, res, next, result, Model) {
  let rows = result.rows || result;

  rows = rows.map(transformResult);

  if (result.rows) {
    result.rows = rows;
    return res.send(result);
  } else {
    return res.send(rows);
  }
}

function handlerGetOne(req, res, next, item) {
  item = transformResult(item);

  return res.send(item);
}

function transformResult(data = {}) {
  if (typeof data.toJSON === 'function') {
    data = data.toJSON();
  }
  let {softcap = 0, hardcap = 0, tokenPriceUsd} = data;

  data.softcapValues = {
    usd: softcap,
    unt: tokenPriceUsd ? softcap / tokenPriceUsd : 0
  };

  data.hardcapValues = {
    usd: hardcap,
    unt: tokenPriceUsd ? hardcap / tokenPriceUsd : 0
  };

  data.crowdsaleAddres = config.crowdsale.address.crowdsaleContract;

  return data;
}

router.route('/:id/approve')
  .put((req, res, next) => {
    let {id} = req.params;
    let user = req.user;
    let isAdmin = user && user.role == 'admin';
    if (!isAdmin) {
      return next({code: 403, message: "Admin only"});
    }
    return Project.update(id, {approved: true})
      .then(reply => {
        res.json({success: reply.approved});
      })
      .catch(err => {
        return next(err);
      });
  });

router.route('/:id(\\d+)/documents')
  .get(AccessMiddleware.check(permissions.GetDocs), (req, res, next) => {
    const {id} = req.params;
    let include = helpers.addPopulatesToFormData(POPULATES_LIST, ["documents"]);
    return Project.get(id, {include})
      .then(reply => {
        if (!reply) {
          return Promise.reject({status: 404, message: 'Project Not Found'})
        }
        if (!reply.documents) {
          return Promise.reject({status: 404, message: 'Project Documents Not Found'})
        }
        // create a file to stream archive data to.
        let output = res; //fs.createWriteStream(DOCUMENTS_BASE_PATH + '/upload/example.zip');
        let archive = archiver('zip', {
          zlib: {level: 9} // Sets the compression level.
        });

        output.on('close', () => {
        });
        // This event is fired when the data source is drained no matter what was the data source.
        // It is not part of this library but rather from the NodeJS Stream API.
        // @see: https://nodejs.org/api/stream.html#stream_event_end
        output.on('end', () => {
        });

        archive.on('warning', (err) => next(err));
        archive.on('error', (err) => next(err));

        // pipe archive data to the file
        archive.pipe(output);

        let ecount = 0;
        reply.documents.forEach(v => {
          if (v.attachmentData && v.attachmentData.path && fs.existsSync(DOCUMENTS_BASE_PATH + v.attachmentData.path)) {
            archive.file(DOCUMENTS_BASE_PATH + v.attachmentData.path, {name: v.attachmentData.filename});
          } else {
            let err = {
              message: "File not found",
              code: 'ENOENT',
              data: v.attachmentData
            };
            archive.append(`${JSON.stringify(err)} `, {name: `error_${++ecount}.json`});
          }
          return null;
        });
        archive.finalize();
        return res;
      })
      .catch(err => next(err));
  });

const handlers = {
  create: handlerCreate,
  update: handlerUpdate,
  getAll: handlerGetAll,
  getOne: handlerGetOne
};

const options = {
  CtrlName,
  permissions,
  route: router,
  handlers,
  POPULATES_LIST,
  localized: true,
  setOwner: true,
  beforeRouteGetAll,
};

module.exports = BaseCtrl(options);
