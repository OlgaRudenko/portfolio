module.exports =  function(pg, db){

	Dish = {
		getAllDishes: function(data, callback){
            db.query('select * from dish',[], function(err, result){
                if (err) {
                    callback(err)
                } else {
                    callback(null, result.rows);
                }
            })
    	},
		addDish: function(data, callback){
            db.query(`insert into dish (name) values ($1)`, [data.dishName], function(err, result){
                if (err) {
                    callback(err)
                } else {
                    callback(null);
                }
            })
    	},
    	editDish: function(data, callback){
            db.query(`update dish 
            set name=$2 
            where id=$1`, [data.dishId, data.dishName], function(err, result){
                if (err) {
                    callback(err)
                } else {
                    callback(null);
                }
            })
    	},
    	deleteDish: function(data, callback){
            db.query(`delete from dish where id=$1`, [data.dishId], function(err, result){
                if (err) {
                    callback(err)
                } else {
                    callback(null);
                }
            })
    	},
        addComposition: function(data, callback){
            db.query(`insert into composition values ($1, $2, $3, $4)`, 
            [data.processingMethodId, data.ingredientId, data.dishId, data.weight], function(err, result){
                if (err) {
                    callback(err)
                } else {
                    callback(null);
                }
            })
        },
        editComposition: function(data, callback){
            db.query(`update composition 
            set weight=$4 
            where dishid=$3 and ingredientid=$2 and processingMethodid=$1`, 
            [data.processingMethodId, data.ingredientId, data.dishId, data.weight], function(err, result){
                if (err) {
                    callback(err)
                } else {
                    callback(null);
                }
            })
        },
        deleteComposition: function(data, callback){
            console.log('we here2')
            db.query(`delete from composition where dishid=$1 and ingredientid=$2 and processingMethodid=$3`, 
            [data.dishId, data.ingredientId, data.processingMethodId], function(err, result){
                if (err) {
                    callback(err)
                } else {
                    callback(null);
                }
            })
        }
	}

	return Dish;
}



   
