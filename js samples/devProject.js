'use strict';
import Router  from 'router';
import BaseCtrl from './index';

import Project           from 'model/Project';
import User              from 'model/User';
import Attachment        from 'model/Attachment';
import ProjectStage      from 'model/ProjectStage';
import ProjectDocument   from 'model/ProjectDocument';
import Contract          from 'model/Crowdsale/Contract';
import helpers            from 'lib/helpers';

import config from '../config';

const router = new Router();

const CtrlName = 'devProject';
const modelName = 'Project';


const permissions = {
  group: CtrlName,
  Read: {key: `${CtrlName}`, role: ['developer'] },
  Create: {key: `${CtrlName}`, role: ['developer'] },
  Update: {key: `${CtrlName}`, role: ['developer'] },
  Delete: {key: `${CtrlName}`, role: ['developer'] }
};

const POPULATES_LIST = {
  'user': {
    model: User.model,
    as: 'userData',
    attributes: ['id', 'firstname', 'lastname', 'username', 'email']
  },
  'images': {
    model: Attachment.model,
    as: 'images',
    through: { attributes: [] },
    attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']}
  },
  'documents': {
    model: ProjectDocument.model,
    as: 'documents',
    attributes: ['id', 'name', 'language'],
    include: [{
      model: Attachment.model,
      as: 'attachmentData',
      attributes: {exclude: ['createdAt', 'updatedAt', 'deletedAt']}
    }]
  },
  'stages': {
    model: ProjectStage.model,
    as: 'stages',
    attributes: ['id', 'name', 'description', 'status']
  },
  'contract': {
    model: Contract.model,
    as: 'contractData'
  }
};

function wrapHandlerCreateUpdate(req, res, next, Model, type = 'create') {
  const formData = Object.assign(req.formData, {approved: undefined});
  let user = req.user;
  let isAdmin = user && user.role == 'admin';

  const populate_list = [];
  let include = helpers.addPopulatesToFormData(POPULATES_LIST, populate_list);
  return User.get(user.id, {include})
    .then(user=>{
      if(!(user||{}).approved){
        return Promise.reject({ status: 403, message: "Current user wasn't approved to create projects"});
      }
      // if (!isAdmin) ?
      formData.userId = user.id;

      let options = [formData];

      if (type === 'update') {
        options.unshift(req.item.id);
      }

      return Model[type](...options)
    })
    .then(project => {
      if (formData.imageIds) {
        return project.setImages(formData.imageIds);
      }
      return project;
    })
    .then(project => {
      return res.json(project);
    })
    .catch(err => next(err));
}

function handlerCreate(...options) {
  return wrapHandlerCreateUpdate.apply(null, options);
}

function handlerUpdate(...options) {
  options.push('update');
  return wrapHandlerCreateUpdate.apply(null, options);
}

function handlerGetAll (req, res, next, result, Model) {
  let rows = result.rows || result;

  rows = rows.map(transformResult);

  if (result.rows) {
    result.rows = rows;
    return res.send(result);
  } else {
    return res.send(rows);
  }
}

function handlerGetOne (req, res, next, item) {
  item = transformResult(item);

  return res.send(item);
}

function transformResult(data = {}) {
  if (typeof data.toJSON === 'function') {
    data = data.toJSON();
  }
  let { softcap = 0, hardcap = 0, tokenPriceUsd } = data;

  data.softcapValues = {
    usd: softcap,
    unt: tokenPriceUsd ? softcap / tokenPriceUsd : 0
  };

  data.hardcapValues = {
    usd: hardcap,
    unt: tokenPriceUsd ? hardcap / tokenPriceUsd : 0
  };

  data.crowdsaleAddres = config.crowdsale.address.crowdsaleContract;

  return data;
}


function beforeRouteGetAll(req, res, next, formData) {
  let user = req.user;
  formData.where = formData.where || {};
  formData.where.userId = user.id;
}

const handlers = {
  create: handlerCreate,
  update: handlerUpdate,
  getAll: handlerGetAll,
  getOne: handlerGetOne
};

const options = {
  CtrlName,
  permissions,
  route: router,
  handlers,
  POPULATES_LIST,
  localized: true,
  setOwner: true,
  modelName,
  beforeRouteGetAll,
};

module.exports = BaseCtrl(options);
