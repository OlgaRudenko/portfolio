var gulp = require('gulp')
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var clean = require('gulp-clean');

gulp.task('watch', function() {
  gulp.watch('src/**', ['scss']);
});

gulp.task('scss', ['copy'], function() {
   gulp.src('./src/css/scss/*.scss') 
    .pipe(sass()) 
    .pipe(gulp.dest('./dest/css')) 
});

gulp.task('clean', function() {
  return gulp.src('dest', {read: false})
    .pipe(clean())
})
 
gulp.task('copy', ['clean'], function() {
  return gulp.src(['./src/**/*.*', '!./src/css/**'])
    .pipe(gulp.dest('./dest'))
})

gulp.task('default', ['scss'])


